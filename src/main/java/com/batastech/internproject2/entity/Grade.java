package com.batastech.internproject2.entity;


import javax.persistence.*;
import java.util.List;

@Entity
public class Grade {
    private int id;
    private String finalGrade;
    private List<Student> student;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(String finalGrade) {
        this.finalGrade = finalGrade;
    }

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "grade")


    public List<Student> getStudent() {
        return student;
    }

    public void setStudent(List<Student> student) {
        this.student = student;
    }





}

package com.batastech.internproject2.entity;

import org.springframework.data.domain.Page;

import java.util.List;
import javax.persistence.*;

/** @author anilmaharjan on 2021-05-26 */
@Entity
public class Student {
  private int id;
  private String name;
  private int age;
  private List<Address> address;
  private List<Contact> contact;
  private Grade grade;



  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }



  @OneToMany(cascade = CascadeType.ALL,mappedBy = "student")
  public List<Address> getAddress() {
    return address;
  }

  public void setAddress(List<Address> address) {
    this.address = address;
  }


  @OneToMany(cascade = CascadeType.ALL,mappedBy = "student")

  public List<Contact> getContact() {
    return contact;
  }

  public void setContact(List<Contact> contact) {
    this.contact = contact;
  }



  @ManyToOne
  /*@JoinTable(name = "gradedetail_studentdetail",
          joinColumns = {@JoinColumn(name = "gradeKey")},
          inverseJoinColumns = {@JoinColumn(name="studentKey")}
  )*/
  public Grade getGrade() {
    return grade;
  }

  public void setGrade(Grade grade) {
    this.grade = grade;
  }




}

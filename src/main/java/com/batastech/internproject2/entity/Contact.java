package com.batastech.internproject2.entity;

import javax.persistence.*;

@Entity
public class Contact {
    private int id;
    private String email;
    private long mobileNumber;
    private long homeContact;
    private long officeContact;
    private Student student;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    public long getHomeContact() {
        return homeContact;
    }

    public void setHomeContact(long homeContact) {
        this.homeContact = homeContact;
    }

    public long getOfficeContact() {
        return officeContact;
    }

    public void setOfficeContact(long officeContact) {
        this.officeContact = officeContact;
    }
    @ManyToOne
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

}

package com.batastech.internproject2.service;

import com.batastech.internproject2.serviceobject.AddressDAO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AddressService {
     int addAddress(AddressDAO address);
    Page<AddressDAO> findAllAddress(int pageSize,int pageNumber);
    AddressDAO findById(int id);

}

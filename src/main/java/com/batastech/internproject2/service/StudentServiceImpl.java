package com.batastech.internproject2.service;

import com.batastech.internproject2.dao.StudentDao;
import com.batastech.internproject2.dao.converter.StudentToStudentDao;
import com.batastech.internproject2.entity.Student;
import com.batastech.internproject2.serviceobject.StudentDAO;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/** @author anilmaharjan on 2021-05-26 */
@Service
public class StudentServiceImpl implements StudentService {
  @Autowired private StudentDao studentDao;
  private static Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);

  @Override
  public int addStudent(StudentDAO request) {
    LOGGER.info("request:" + request.getName());
    Student student = this.studentDao.save(new StudentToStudentDao().toEntity(request));
    if (student != null) {
      return student.getId();
    }
    return 0;
  }

  @Override
  public Page<StudentDAO> findAllStudent(int pageNumber, int pageSize) {
    Pageable paging = PageRequest.of(pageNumber, pageSize);
    Page<Student> page = this.studentDao.findAll(paging);
    return new PageImpl(
        new StudentToStudentDao().toServiceObjectList(page.getContent()),
        page.getPageable(),
        page.getTotalElements());
  }

  @Override
  public StudentDAO findById(int id) {
    Optional<Student> optionalStudent = this.studentDao.findById(id);
    if (optionalStudent.isEmpty()) {
      return null;
    }
    return new StudentToStudentDao().toServiceObject(optionalStudent.get());
  }
}

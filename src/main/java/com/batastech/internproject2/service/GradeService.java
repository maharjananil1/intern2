package com.batastech.internproject2.service;

import com.batastech.internproject2.serviceobject.GradeDAO;
import org.springframework.data.domain.Page;

public interface GradeService {
    int addGrade(GradeDAO gradeDAO);
    Page<GradeDAO> findAllGrade(int pageNumber,int pageSize);
    GradeDAO findById(int id);
}

package com.batastech.internproject2.service;

import com.batastech.internproject2.serviceobject.ContactDAO;
import org.springframework.data.domain.Page;

public interface ContactService {
    int addContact(ContactDAO contact);
    Page<ContactDAO> findAllContact(int pageNumber, int pageSize);
    ContactDAO findById(int id);
}

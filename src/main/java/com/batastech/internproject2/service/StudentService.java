package com.batastech.internproject2.service;

import com.batastech.internproject2.serviceobject.StudentDAO;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author anilmaharjan on 2021-05-26
 */
public interface StudentService {
    int addStudent(StudentDAO student);
    Page<StudentDAO> findAllStudent(int pageNumber, int pageSize);
    StudentDAO findById(int id);
}

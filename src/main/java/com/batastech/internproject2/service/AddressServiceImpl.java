package com.batastech.internproject2.service;

import com.batastech.internproject2.dao.AddressDao;
import com.batastech.internproject2.dao.converter.AddressToAddressDao;
import com.batastech.internproject2.entity.Address;
import com.batastech.internproject2.serviceobject.AddressDAO;
import com.batastech.internproject2.serviceobject.StudentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService{
    @Autowired
    private AddressDao addressDao;

    @Override
    public int addAddress(AddressDAO address) {
        Address address1 = this.addressDao.save(new AddressToAddressDao().toEntity(address));
        if(address1 != null) {
            return address1.getId();
        }
        return 0;
    }



    @Override
    public Page<AddressDAO> findAllAddress(int pageSize, int pageNumber) {
        Pageable pageable = PageRequest.of(pageSize,pageNumber);
        Page<Address> page = this.addressDao.findAll(pageable);
        return new PageImpl(
                new AddressToAddressDao().toServiceObjectList(page.getContent()),
                page.getPageable(),
                page.getTotalElements()
        );
    }

    @Override
    public AddressDAO findById(int id) {
        Optional<Address> optionalAddress = this.addressDao.findById(id);
        if (optionalAddress.isEmpty()){
            return null;
        }
        return new AddressToAddressDao().toServiceObject(optionalAddress.get());
    }
}

package com.batastech.internproject2.service;

import com.batastech.internproject2.dao.GradeDao;
import com.batastech.internproject2.dao.converter.GradeToGradeDao;
import com.batastech.internproject2.entity.Grade;
import com.batastech.internproject2.serviceobject.GradeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GradeServiceImpl implements GradeService{
    @Autowired
    private GradeDao gradeDao;

    @Override
    public int addGrade(GradeDAO gradeDAO) {
        /*Grade g;
        g=new GradeToGradeDao().toEntity(gradeDAO);
        Grade g1;
        g1 = this.gradeDao.save(g);*/

        Grade grade = this.gradeDao.save(new GradeToGradeDao().toEntity(gradeDAO));
        if(grade != null){
            return grade.getId();
        }
        return 0;
    }

    @Override
    public Page<GradeDAO> findAllGrade(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize);
        Page<Grade> page = this.gradeDao.findAll(pageable);
        return new PageImpl(
                new GradeToGradeDao().toServiceObjectList(page.getContent()),
                page.getPageable(),
                page.getTotalElements()
        );

    }

    @Override
    public GradeDAO findById(int id) {
        Optional<Grade> optionalGrade = this.gradeDao.findById(id);
        if(optionalGrade.isEmpty()){
            return null;
        }
        return new GradeToGradeDao().toServiceObject(optionalGrade.get());
    }
}

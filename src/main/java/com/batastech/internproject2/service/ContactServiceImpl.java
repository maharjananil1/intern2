package com.batastech.internproject2.service;

import com.batastech.internproject2.dao.ContactDao;
import com.batastech.internproject2.dao.converter.ContactToContactDao;
import com.batastech.internproject2.entity.Contact;
import com.batastech.internproject2.serviceobject.ContactDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service

public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactDao contactDao;

    @Override
    public int addContact(ContactDAO contact) {
        Contact contact1 = this.contactDao.save(new ContactToContactDao().toEntity(contact));
        if(contact1 != null){
            return contact1.getId();
        }
        return 0;
    }

    @Override
    public Page<ContactDAO> findAllContact(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize);
        Page<Contact> page = this.contactDao.findAll(pageable);
        return  new PageImpl(
            new ContactToContactDao().toServiceObjectList(page.getContent()),
        page.getPageable(),
        page.getTotalElements());

    }

    @Override
    public ContactDAO findById(int id) {
        Optional<Contact> optionalContact = this.contactDao.findById(id);
        if (optionalContact.isEmpty()) {
            return null;
        }
        return new ContactToContactDao().toServiceObject(optionalContact.get());
    }
}

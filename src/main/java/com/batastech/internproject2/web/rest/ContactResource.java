package com.batastech.internproject2.web.rest;

import com.batastech.internproject2.service.ContactService;
import com.batastech.internproject2.serviceobject.ContactDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("contact")
public class ContactResource {
    @Autowired
    private ContactService contactService;

    @PostMapping
    public ResponseEntity<Integer> addContact(@RequestBody ContactDAO contactDAO){
        return new ResponseEntity<>(this.contactService.addContact(contactDAO), HttpStatus.OK);
    }
    @GetMapping
    public ResponseEntity<Page<ContactDAO>>  findAllContact(
            @RequestParam (defaultValue = "0") int pageNumber,
            @RequestParam (defaultValue = "3")int pageSize
    ){
        return new ResponseEntity<>(this.contactService.findAllContact(pageNumber,pageSize),HttpStatus.OK);

    }
    @GetMapping(value = "{id}")
    public ResponseEntity<ContactDAO> findById(int id){
        return new ResponseEntity<>(this.contactService.findById(id),HttpStatus.OK);
    }
}

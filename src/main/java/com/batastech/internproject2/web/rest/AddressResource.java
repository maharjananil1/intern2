package com.batastech.internproject2.web.rest;

import com.batastech.internproject2.service.AddressService;
import com.batastech.internproject2.service.ContactService;
import com.batastech.internproject2.service.ContactServiceImpl;
import com.batastech.internproject2.serviceobject.AddressDAO;
import com.batastech.internproject2.serviceobject.ContactDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("address")
public class AddressResource {
    @Autowired
    private AddressService addressService;
    @PostMapping
    public ResponseEntity<Integer> addAddress(@RequestBody AddressDAO addressDAO){
        return new ResponseEntity<>(this.addressService.addAddress(addressDAO), HttpStatus.OK) ;
    }
    @GetMapping
    public ResponseEntity<Page<AddressDAO>> findAllAddress(
            @RequestParam (defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "3") int pageSize
    ){


        return new ResponseEntity<>(this.addressService.findAllAddress(pageNumber,pageSize),HttpStatus.OK);
    }
    @GetMapping(value = "{id}")
    public ResponseEntity<AddressDAO> findById(@PathVariable int id){
        return new ResponseEntity<>(this.addressService.findById(id),HttpStatus.OK);
    }
}

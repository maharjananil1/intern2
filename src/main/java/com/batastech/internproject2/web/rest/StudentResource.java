package com.batastech.internproject2.web.rest;

import com.batastech.internproject2.dao.AddressDao;
import com.batastech.internproject2.entity.Address;
import com.batastech.internproject2.service.AddressService;
import com.batastech.internproject2.service.AddressServiceImpl;
import com.batastech.internproject2.service.StudentService;
import com.batastech.internproject2.serviceobject.AddressDAO;
import com.batastech.internproject2.serviceobject.StudentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/** @author anilmaharjan on 2021-05-05 */
@RestController
@RequestMapping("student")
public class
StudentResource {
  @Autowired private StudentService studentService;


  @GetMapping(value = "test")
  public ResponseEntity<String> test() {
    return new ResponseEntity<>("Hello World", HttpStatus.OK);
  }

  @PostMapping(value = "/save")
  public ResponseEntity<Integer> save(@RequestBody StudentDAO studentDAO) {
    return new ResponseEntity<>(studentService.addStudent(studentDAO), HttpStatus.OK);
  }

  @GetMapping
  public ResponseEntity<Page<StudentDAO>> findAllStudent(
      @RequestParam(defaultValue = "0") int pageNumber,
      @RequestParam(defaultValue = "3") int pageSize) {
    return new ResponseEntity<>(studentService.findAllStudent(pageNumber, pageSize), HttpStatus.OK);
  }
 /* @PostMapping(value = "/Address")
  public ResponseEntity<StudentDAO> saveStudent(@RequestBody StudentDAO studentDAO){
  return new ResponseEntity<StudentDAO>(studentService.addStudent(studentDAO),HttpStatus.OK);
 }*/
/*  @PostMapping(value = "/address")
  public  ResponseEntity<Integer> saveAddress(@RequestBody AddressDAO addressDao){
    return new ResponseEntity<>(addressService.addAddress(addressDao),HttpStatus.OK);
  }*/
  @GetMapping(value = "{id}")
  public ResponseEntity<StudentDAO> findById(@PathVariable int id) {
    return new ResponseEntity<>(studentService.findById(id), HttpStatus.OK);
  }
}

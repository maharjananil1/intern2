package com.batastech.internproject2.web.rest;

import com.batastech.internproject2.entity.Grade;
import com.batastech.internproject2.service.GradeService;
import com.batastech.internproject2.serviceobject.GradeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("grade")
public class GradeResource {

    @Autowired
    private GradeService gradeService;

    @PostMapping
    public int addGrade(@RequestBody GradeDAO gradeDAO){
        return this.gradeService.addGrade(gradeDAO);
    }
    @GetMapping
    public Page<GradeDAO> getAllGrade(
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "3") int pageSize
    ){
        return this.gradeService.findAllGrade(pageNumber, pageSize);
    }
    @GetMapping(value = "{id}")
    public GradeDAO getById(int id){
        return this.gradeService.findById(id);
    }
}

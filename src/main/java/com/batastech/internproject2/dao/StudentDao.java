package com.batastech.internproject2.dao;

import com.batastech.internproject2.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/** @author anilmaharjan on 2021-05-26 */
@Repository
public interface StudentDao extends JpaRepository<Student, Integer> {}

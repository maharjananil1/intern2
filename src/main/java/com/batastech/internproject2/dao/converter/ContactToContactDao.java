package com.batastech.internproject2.dao.converter;

import com.batastech.internproject2.entity.Contact;
import com.batastech.internproject2.serviceobject.ContactDAO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

public class ContactToContactDao {
    public Contact toEntity(ContactDAO contactDAO){
        Contact contact = new Contact();
        contact.setId(contactDAO.getId());
        contact.setEmail(contactDAO.getEmail());
        contact.setMobileNumber(contactDAO.getMobileNumber());
        contact.setHomeContact(contactDAO.getHomeContact());
        contact.setOfficeContact(contactDAO.getOfficeContact());
        contact.setStudent(new StudentToStudentDao().toEntity(contactDAO.getStudent()));


        return contact;
    }
    public ContactDAO toServiceObject(Contact contact){
        ContactDAO contactDao = new ContactDAO();
        contactDao.setId(contact.getId());
        contactDao.setEmail(contact.getEmail());
        contactDao.setMobileNumber(contact.getMobileNumber());
        contactDao.setHomeContact(contact.getHomeContact());
        contactDao.setOfficeContact(contact.getOfficeContact());
        contactDao.setStudent(new StudentToStudentDao().toServiceObject(contact.getStudent()));
        return contactDao;

    }
    public List<Contact> toEntityList(List<ContactDAO> contactDAOList ){
        return contactDAOList.stream().map(this::toEntity).collect(Collectors.toList());

    }
    public List<ContactDAO> toServiceObjectList(List<Contact> contactList){
        return contactList.stream().map(this::toServiceObject).collect(Collectors.toList());

    }
}

package com.batastech.internproject2.dao.converter;

import com.batastech.internproject2.entity.Student;
import com.batastech.internproject2.serviceobject.StudentDAO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

/** @author anilmaharjan on 2021-05-26 */
public class StudentToStudentDao {
  public Student toEntity(StudentDAO studentDAO) {
    Student student = new Student();
    student.setId(studentDAO.getId());
    student.setName(studentDAO.getName());
    student.setAge(studentDAO.getAge());
    //student.setAddress(new AddressToAddressDao().toEntityList(studentDAO.getAddress()));
    //student.setContact(new ContactToContactDao().toEntityList(studentDAO.getContact()));
    student.setGrade(new GradeToGradeDao().toEntity(studentDAO.getGrade()));
    return student;
  }

  public StudentDAO toServiceObject(Student student) {
    StudentDAO studentDao = new StudentDAO();
    studentDao.setId(student.getId());
    studentDao.setName(student.getName());
    studentDao.setAge(student.getAge());
   // studentDao.setAddress(new AddressToAddressDao().toServiceObjectList(student.getAddress()));
    //studentDao.setContact(new ContactToContactDao().toServiceObjectList(student.getContact()));
    studentDao.setGrade(new GradeToGradeDao().toServiceObject(student.getGrade()));
    return studentDao;
  }

  public List<Student> toEntityList(List<StudentDAO> studentDAOList) {
    return studentDAOList.stream().map(this::toEntity).collect(Collectors.toList());
  }

  public List<StudentDAO> toServiceObjectList(List<Student> studentList) {
    return studentList.stream().map(this::toServiceObject).collect(Collectors.toList());
  }
}

package com.batastech.internproject2.dao.converter;

import com.batastech.internproject2.entity.Address;
import com.batastech.internproject2.serviceobject.AddressDAO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

/** @author anilmaharjan on 2021-05-26 */
public class AddressToAddressDao {
  public Address toEntity(AddressDAO addressDAO) {
    Address address = new Address();
    address.setId(addressDAO.getId());
    address.setCity(addressDAO.getCity());
    address.setProvince(addressDAO.getProvince());
    address.setDistrict(addressDAO.getDistrict());
    address.setCountry(addressDAO.getCountry());
    address.setStreet(addressDAO.getStreet());
    address.setType(addressDAO.getType());
    address.setStudent(new StudentToStudentDao().toEntity(addressDAO.getStudent()));
    return address;
  }

  public AddressDAO toServiceObject(Address address) {
    AddressDAO addressDao = new AddressDAO();
    addressDao.setId(address.getId());
    addressDao.setCity(address.getCity());
    addressDao.setProvince(address.getProvince());
    addressDao.setDistrict(address.getDistrict());
    addressDao.setCountry(address.getCountry());
    addressDao.setStreet(address.getStreet());
    addressDao.setType(address.getType());
    addressDao.setStudentDAO(new StudentToStudentDao().toServiceObject(address.getStudent()));
    return addressDao;
  }

  public List<Address> toEntityList(List<AddressDAO> addressDAOList) {
    return addressDAOList.stream().map(this::toEntity).collect(Collectors.toList());
  }

  public List<AddressDAO> toServiceObjectList(List<Address> addressList) {
    return addressList.stream().map(this::toServiceObject).collect(Collectors.toList());
  }
}

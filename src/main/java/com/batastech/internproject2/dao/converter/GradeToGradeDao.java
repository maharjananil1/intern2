package com.batastech.internproject2.dao.converter;

import com.batastech.internproject2.entity.Address;
import com.batastech.internproject2.entity.Grade;
import com.batastech.internproject2.serviceobject.AddressDAO;
import com.batastech.internproject2.serviceobject.GradeDAO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

public class GradeToGradeDao {
    public Grade toEntity(GradeDAO gradeDAO){
        Grade grade = new Grade();
        grade.setId(gradeDAO.getId());
        grade.setFinalGrade(gradeDAO.getFinalGrade());
        //grade.setStudent(new StudentToStudentDao().toEntityList(gradeDAO.getStudent()));
        return grade;
    }
    public GradeDAO toServiceObject(Grade grade){
        GradeDAO gradeDao= new GradeDAO();
        gradeDao.setId(grade.getId());
        gradeDao.setFinalGrade(grade.getFinalGrade());
        //gradeDao.setStudent(new StudentToStudentDao().toServiceObjectList(grade.getStudent()));
        return gradeDao;

    }

    public List<Grade> toEntityList(List<GradeDAO> gradeDAOList) {
        return gradeDAOList.stream().map(this::toEntity).collect(Collectors.toList());
    }
    public List<GradeDAO> toServiceObjectList(List<Grade> gradeList){
        return gradeList.stream().map(this::toServiceObject).collect(Collectors.toList());
    }
}

package com.batastech.internproject2.dao;

import com.batastech.internproject2.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/** @author anilmaharjan on 2021-05-26 */
@Repository
public interface AddressDao extends JpaRepository<Address, Integer> {}

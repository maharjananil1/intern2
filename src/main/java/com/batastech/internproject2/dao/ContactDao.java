package com.batastech.internproject2.dao;

import com.batastech.internproject2.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactDao extends JpaRepository <Contact,Integer> {
}

package com.batastech.internproject2.dao;

import com.batastech.internproject2.entity.Grade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GradeDao extends JpaRepository<Grade,Integer> {
}

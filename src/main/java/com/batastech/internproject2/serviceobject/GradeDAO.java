package com.batastech.internproject2.serviceobject;

import java.util.List;

public class GradeDAO {
    private int id;
    private String finalGrade;
    private List<StudentDAO> student;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(String finalGrade) {
        this.finalGrade = finalGrade;
    }

    public List<StudentDAO> getStudent() {
        return student;
    }

    public void setStudent(List<StudentDAO> student) {
        this.student = student;
    }
}

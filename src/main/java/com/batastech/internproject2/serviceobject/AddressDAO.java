package com.batastech.internproject2.serviceobject;

/**
 * @author anilmaharjan on 2021-05-26
 */
public class AddressDAO {
    private int id;
    private String city;
    private String district;
    private String province;
    private String country;
    private String street;
    private String type;
    private StudentDAO student;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public StudentDAO getStudent() {
        return student;
    }

    public void setStudentDAO(StudentDAO student) {
        this.student = student;
    }
}

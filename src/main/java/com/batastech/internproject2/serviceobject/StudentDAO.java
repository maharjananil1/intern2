package com.batastech.internproject2.serviceobject;

import java.util.List;

/** @author anilmaharjan on 2021-05-26 */
public class StudentDAO {
  private int id;
  private String name;
  private int age;
  private List<AddressDAO> address;
  private List<ContactDAO> contact;
  private GradeDAO grade;



  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public List<AddressDAO> getAddress() {
    return address;
  }

  public void setAddress(List<AddressDAO> address) {
    this.address = address;
  }
  public List<ContactDAO> getContact() {
    return contact;
  }

  public void setContact(List<ContactDAO> contact) {
    this.contact = contact;
  }

  public GradeDAO getGrade() {
    return grade;
  }

  public void setGrade(GradeDAO grade) {
    this.grade = grade;
  }
}

package com.batastech.internproject2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Internproject2Application {
	public static void main(String[] args) {
		SpringApplication.run(Internproject2Application.class, args);
	}
}
